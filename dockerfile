FROM python:3.9.6-slim

LABEL maintainer="s@lobanov.pro"

# install deps and clear cache

RUN apt-get update && apt-get install -y \
    git \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# clone repo
RUN git clone https://github.com/adterskov/geekbrains-conteinerization.git

RUN pip3 install --no-cache-dir -r geekbrains-conteinerization/homework/2.docker/python/requirements.txt

WORKDIR geekbrains-conteinerization/homework/2.docker/python/

EXPOSE 8080
CMD [ "python", "./app.py" ]